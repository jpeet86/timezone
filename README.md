# React Timezone comparison

User can select the country of origin and then check the timezone in another country

## Tasks Done

* ~~Setup project using~~
* ~~Get a ticking clock~~
* ~~Add a `<select>` field looping through dummy countries~~
* ~~Change time on `<select>` change~~
* ~~Use API to populate `<select>` `<option>`'s~~
* ~~Add to clock components~~
* ~~Compare times from both components~~
* ~~Change time comparisons to compare `Date` object~~
* ~~Refactor code~~
* ~~Change `<select>` to text `<input>`~~

## Tasks to do

* Finish styling `selector`
* Use a different API for country/city `<select>`
* Build project and deploy 



