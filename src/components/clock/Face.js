import React, { Component } from 'react';

class Face extends Component {
  
  constructor (props) {
    super(props);
    this.state = {
      d: new Date(),
      handSecs: 0,
      handMins: 0,
      handHours: 0
    }
    this.updateTime = this.updateTime.bind(this)
    this.addHours = this.addHours.bind(this)
  }

  componentDidMount () {
    this.interval = setInterval(this.updateTime.bind(this), 1000)
  }

  // Clock tick
  updateTime() {
    this.setState({
      d: new Date(),
      handSecs: (360 / 60) * this.state.d.getSeconds(),
      handMins: (360 / 60) * this.state.d.getMinutes(),
      handHours: (360 / 12) * this.state.d.getHours()
    })
  }

  // Change hours
  addHours (h){
    h = parseInt(h)
    this.state.d.setHours((this.state.d.getHours() + h))
  }

  render () {
    this.addHours(this.props.timeDiff)
    
    let handSecsStyle = {transform: 'rotate(' + this.state.handSecs + 'deg)'}
    let handMinsStyle = {transform: 'rotate(' + this.state.handMins + 'deg)'}
    let handHoursStyle = {transform: 'rotate(' + this.state.handHours + 'deg)'}

    return (
      <div>
        <div className="clock">
          <div className="hand hand-hours" style={handHoursStyle}><span>{this.state.d.getHours()}</span></div>
          <div className="hand hand-mins" style={handMinsStyle}><span>{this.state.d.getMinutes()}</span></div>
          <div className="hand hand-secs" style={handSecsStyle}><span>{this.state.d.getSeconds()}</span></div>
        </div>
      </div>
    );
  }
}

export default Face;
