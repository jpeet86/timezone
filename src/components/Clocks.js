import React, { Component } from 'react';
import Clock from './Clock';

class Clocks extends Component {

  constructor () {
    super()
    this.state = {
    	clocks: [new Date(), new Date()],
    	timeDiff: 0
    }
    this.addOffset = this.addOffset.bind(this)
  }

  addOffset (index, timestamp) {

    let clocks = this.state.clocks.slice(0)
    let timestampSum = 0

    clocks[index] = new Date(timestamp * 1000)

    if(clocks[0] > 0 ) {

      if(clocks[1] > 0) {
        timestampSum = (Math.round((clocks[0] - clocks[1]) / 1000 / 60 / 60)) * -1
      } else {
        timestampSum = Math.round((clocks[0] - clocks[1]) / 1000 / 60 / 60)
      }

    } else {
      timestampSum = (Math.round((clocks[0] - clocks[1]) / 1000 / 60 / 60)) * -1
    }

    this.setState ({
    	clocks,
    	timeDiff: timestampSum
    })

  }

  render () {

    return (
      <div className="pa4">
        <h2><span>{this.state.timeDiff} hour(s)</span> time difference</h2>
        <Clock addOffset={this.addOffset} index={0} />
        <Clock addOffset={this.addOffset} index={1} />
      </div>
    );
  }

}

export default Clocks;
