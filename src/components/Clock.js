import Search from 'react-search';
import Select from 'react-select';
import React, { Component } from 'react';
import Face from './clock/Face';

import 'react-select/dist/react-select.css';

// Country select start
class Clock extends Component {

  constructor () {
    super()
    this.state = {
      gmtOffset: 0,
      data: {zones: []},
      countrySelect: 'GB'
    }
  }

  // Request data from Timezone API
  componentDidMount () {
    fetch("http://api.timezonedb.com/v2/list-time-zone?key=6KFSXAA33B1V&format=json")
      .then(response => response.json())
      .then(json => this.setState({data: json}))
  }

  // Update country selected on select change
  handleChange (selected) {
    const zones = this.state.data.zones
    const findZone = zones.find(zone => zone.countryCode === selected.value)
    this.setState (
      {
        gmtOffset: findZone.gmtOffset / 60 / 60,
        countrySelect: selected.value
      }
    )

    this.props.addOffset(this.props.index, findZone.timestamp)
  }

  render () {

    const zones = this.state.data.zones;
    let items = [];

    const countries = zones.map((country, i) => {
        items.push({value:country.countryCode, label:country.countryName, clearableValue: false})
      }
    )

    return (
      <div className="w-third">
        <Select
          name="form-field-name"
          value={this.state.countrySelect}
          options={items}
          onChange={this.handleChange.bind(this)}
        />
        <Face timeDiff={this.state.gmtOffset} />
      </div>
    )
  }
}

export default Clock
