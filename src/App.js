import React, { Component } from 'react';
import Clocks from './components/Clocks';
//import logo from './logo.svg';

class App extends Component {

  render() {

    return (
      <div className="pa4">
        <Clocks />
      </div>
    );
  }
}

export default App;